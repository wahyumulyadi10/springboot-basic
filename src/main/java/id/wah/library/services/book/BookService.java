package id.wah.library.services.book;

import id.wah.library.payloads.request.BookRequest;
import id.wah.library.payloads.response.ResponseData;

public interface BookService {
    
    // Create book
    ResponseData createBookService(BookRequest request);

    // Read all books
    ResponseData getBookService(Boolean status);

    // read book by id
    ResponseData getBookByIdService(Long idBook, Boolean status);

    // update book
    ResponseData updateBookByIdService(Long idBook, BookRequest request);

    // delete book
    ResponseData deleteBook(Long idBook);


}
