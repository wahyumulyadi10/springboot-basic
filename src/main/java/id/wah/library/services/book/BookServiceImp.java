package id.wah.library.services.book;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import id.wah.library.models.Books;
import id.wah.library.payloads.request.BookRequest;
import id.wah.library.payloads.response.ResponseData;
import id.wah.library.repositories.BookRepositories;

@Service
public class BookServiceImp implements BookService {

    // instance object yang dibutuhkan
    @Autowired
    private BookRepositories bookRepositories;

    // private Books books = new Books();

    @Override
    public ResponseData createBookService(BookRequest request) {
        
        // TODO Auto-generated method stub
         Books books = new Books();

        if (request.getTitle() != null) {
            books.setTitle(request.getTitle());

        }
        if (request.getCategory() != null) {
            books.setCategory(request.getCategory());

        }
        if (request.getAuthor() != null) {
            books.setAuthor(request.getAuthor());
        }
        if (request.getPublisher() != null) {
            books.setPublisher(request.getPublisher());
        }
        if (request.getYear() != null) {
            books.setYear(request.getYear());
        }
        bookRepositories.save(books);
        ResponseData responseData = new ResponseData(HttpStatus.CREATED.value(), "success", books);

        return responseData;
    }

    @Override
    public ResponseData deleteBook(Long idBook) {
        // TODO Auto-generated method stub
        Optional<Books> books= bookRepositories.findById(idBook);
        ResponseData responseData;
        if (books.isPresent()) {
            Books book = books.get();
            book.setIsDeleted(true);
            bookRepositories.save(book);
            responseData = new ResponseData(HttpStatus.OK.value(), "success", null);
            // bookRepositories.deleteById(idBook);
           
        }else{
            responseData = new  ResponseData(HttpStatus.NOT_FOUND.value(),"Not Found",null);
        }
        return responseData;
    }

    @Override
    public ResponseData getBookByIdService(Long idBook, Boolean status) {
        // TODO Auto-generated method stub
        Optional<Books> books;
        ResponseData responseData;
        if (status == null) {
            books = bookRepositories.findById(idBook);
        }else{
            books = bookRepositories.findByIdAndIsDeleted(idBook,status);
        }

     
      
        if (books.isPresent()) {
             responseData = new ResponseData(HttpStatus.OK.value(),"success",books.get());
        } else {
             responseData = new  ResponseData(HttpStatus.NOT_FOUND.value(),"Not Found",null);
        }
        return responseData;
    }

    @Override
    public ResponseData getBookService(Boolean status) {
        List<Books> books ;
        if (status == null) {
            books = bookRepositories.findAll();
        } else {
            books = bookRepositories.findByIsDeleted(status);
        }
        // TODO Auto-generated method stub 
        // List<Books> books = bookRepositories.findAll();
    
        return new ResponseData(HttpStatus.OK.value(),"success",books);

    }

    @Override
    public ResponseData updateBookByIdService(Long idBook, BookRequest request) {

        Optional<Books> books= bookRepositories.findById(idBook);
        ResponseData responseData ;
        if (books.isPresent()) {
            Books book = books.get();

            if (request.getTitle() != null) {
                book.setTitle(request.getTitle());
    
            }
            if (request.getCategory() != null) {
                book.setCategory(request.getCategory());
    
            }
            if (request.getAuthor() != null) {
                book.setAuthor(request.getAuthor());
            }
            if (request.getPublisher() != null) {
                book.setPublisher(request.getPublisher());
            }
            if (request.getYear() != null) {
                book.setYear(request.getYear());
            }
            bookRepositories.save(book);
            responseData = new ResponseData(HttpStatus.OK.value(), "success", books);
    
            return responseData;
        } else {
            responseData = new  ResponseData(HttpStatus.NOT_FOUND.value(),"Not Found",null);
            return responseData;

        }
        // TODO Auto-generated method stub
    }

}
