package id.wah.library.models;

import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;





@Entity
@Table(name = "books")
@Data
@NoArgsConstructor
public class Books {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(length = 255)
    private String title;
    private String publisher;
    private String author;
    private String year;
    private String category;
    
    @CreationTimestamp
    @JsonIgnore
    private LocalDateTime  created_at,updated_at;
    
    @UpdateTimestamp
    @JsonIgnore
    private LocalDateTime deleted_at; 
    
    private Boolean isDeleted = false;
    //custom cunstroctor
    public Books(String title, String publisher, String author, String year, String category) {
        this.title = title;
        this.publisher = publisher;
        this.author = author;
        this.year = year;
        this.category = category;
    }

     
}
