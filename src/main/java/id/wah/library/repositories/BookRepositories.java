package id.wah.library.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.wah.library.models.Books;

@Repository
public interface BookRepositories extends JpaRepository<Books,Long> {
    List<Books> findByIsDeleted(Boolean isDeleted);

    Optional<Books> findByIdAndIsDeleted(Long idBook,Boolean isDeleted);
    
    
}
