package id.wah.library.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/hello")
public class GreetingController {

    @GetMapping("/")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
      return String.format("Hello %s!", name);
    }
    @PostMapping("/post")
    public String postHello(@RequestParam(value = "name", defaultValue = "World") String name) {
      return String.format("Hello %s!", name);
    }
    
    @PostMapping("/post/{sapa}/{number}")
    public String helloPost(@PathVariable("sapa") String sapa, @PathVariable("number") Integer number){
        return "ini method post params : " +sapa+""+number;
    }

    @PutMapping("/put")
    public String helloPut(@RequestBody String kalimat){
        return "ini adalah method put, kalimatnya :"+kalimat;
    }

}
